BASE=whitepaper_inhom

default:
	pdflatex ${BASE}; bibtex ${BASE}; pdflatex ${BASE}; pdflatex ${BASE}

clean:
	rm -f *.aux *.bbl *.log *.blg *.dvi *.out

# last line
